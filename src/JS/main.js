(function() {
  function loadScript(url, callback) {
    var script = document.createElement("script");
    script.type = "text/javascript";

    if (script.readyState) {
      //IE
      script.onreadystatechange = function() {
        if (script.readyState == "loaded" || script.readyState == "complete") {
          script.onreadystatechange = null;
          callback();
        }
      };
    } else {
      //Others
      script.onload = function() {
        callback();
      };
    }

    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
  }

  function loadCSS(url) {
    var head = document.getElementsByTagName("head")[0];

    var style = document.createElement("link");
    style.href = url;
    style.type = "text/css";
    style.rel = "stylesheet";
    head.append(style);
  }

  function setObserver($, data) {
    // We listen to the mutations on the '__next' div
    let observer = new MutationObserver(function() {
      if(window.location.href.includes('start')) {
        insertTermsBox($, data);
      }
    });

    // configuration of the observer:
    var config = { attributes: true, childList: true, characterData: true, subtree: true };

    // pass in the target node, as well as the observer options
    observer.observe(document.querySelector('#__next'), config);
  }

  function insertTermsBox($, data) {
    if($('.terms-panel').length > 0) {
      return;
    }

    const insertSpot = $('input[name ="billingAddress.id_number"], input[name ="billing_id_number"]');

    if(insertSpot.length > 0) {
      // Set the terms agreement area HTML
      const termsHtml = getTermsHTML(data.display_text, data.terms_text.replace(/\r\n/g, "<br />"));

      // Insert the HTML before the buy button
      const lastPanel = insertSpot.closest('.panel-with-header');
      $(termsHtml).insertAfter(lastPanel);

      const submitBtns = $('button[type=submit]');

      // Disable the buy button
      $(submitBtns).prop("disabled", true);
      $(submitBtns).css("opacity", 0.5);
      $(submitBtns).css('pointer-events', 'none');

      // Listen to changes on the terms checkbox
      $("#terms-checkbox").on("change", function() {
        if (this.checked) {
          $("#terms-checkbox-check").show();
          $(submitBtns).prop("disabled", false);
          $(submitBtns).css("opacity", 1.0);
          $(submitBtns).css('pointer-events', '');
        } else {
          $("#terms-checkbox-check").hide();
          $(submitBtns).prop("disabled", true);
          $(submitBtns).css("opacity", 0.5);
          $(submitBtns).css('pointer-events', 'none');
        }
      });
    }
  }

  var myAppJavaScript = function($) {
    // If there is no jQuery on page, we set it
    // We need to do it so the jquery.modal can work
    if(!window.jQuery) {
      window.jQuery = $;
    }
    
    loadScript(
      "//cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js",
      function() {
        loadCSS("//s3.amazonaws.com/coopers-shop/terms/terms.min.css");

        // Check if we are on the first checkout page 
        if (window.location.href.includes("checkout") && window.location.href.includes("start")) {
          // Get the app settings
          var url =
            "https://coopers-shop-app-terms-of-use.herokuapp.com/stores/" +
            LS.store.id +
            "/settings";
          $.get(url, function(data) {
            // Check if the app is active
            if (data.is_active) {
              setObserver($, data);
            } 
          });
        }
      }
    );
  };

  var getTermsHTML = function(displayText, terms) {
    return `
      <div id="ex1" class="terms modal">
        <p>
          ${terms}
        </p>
        <a href="#" rel="modal:close">Fechar</a>
      </div>
      
      <div class="form-box terms-panel" style="-webkit-tap-highlight-color: transparent;
      line-height: 1.42857;
      font-size: 13px;
      font-weight: 300;
      box-sizing: border-box;
      margin-bottom: 15px;
      overflow: hidden;
      width: 100%;
      border-radius: 0;
      padding: 10px 20px 20px 20px;
      border: 1px solid;">
        <h1 style="-webkit-tap-highlight-color: transparent;
        box-sizing: border-box;
        line-height: 22px;
        padding: 10px 0;
        margin: 0;
        text-align: center;
        font-size: 16px;
        margin-bottom: 15px;
        clear: both;
        text-shadow: none;
        border-bottom: 1px solid #e6e6e6;">Termos de Compra</h1>
        <label for="terms-checkbox" style="-webkit-tap-highlight-color: transparent;
        line-height: 1.42857;
        color: #333333;
        box-sizing: border-box;
        display: inline-block;
        max-width: 100%;
        width: 100%;
        padding: 10px;
        border-radius: 4px;
        font-size: 13px;
        font-weight: 400;
        margin-bottom: 0;
        cursor: pointer;
        border: 1px solid #e1e1e1;">
          <span style="-webkit-tap-highlight-color: transparent;
          line-height: 1.42857;
          color: #333333;
          font-size: 13px;
          font-weight: 400;
          cursor: pointer;
          box-sizing: border-box;
          position: relative;
          display: inline-block;
          margin-right: 10px;
          width: initial;
          float: left;">
            <input style="-webkit-tap-highlight-color: transparent;
            color: inherit;
            font: inherit;
            font-family: inherit;
            font-size: inherit;
            box-sizing: border-box;
            line-height: normal;
            position: absolute;
            outline: 0;
            left: 0;
            top: 0;
            padding: 0;
            width: 16px;
            height: 16px;
            border: none;
            margin: 0;
            opacity: 0;
            z-index: 1;
            cursor: pointer;" id="terms-checkbox" type="checkbox">
            <span style="-webkit-tap-highlight-color: transparent;
            line-height: 1.42857;
            color: #333333;
            font-size: 13px;
            font-weight: 400;
            cursor: pointer;
            box-sizing: border-box;
            position: relative;
            display: block;
            width: 17px;
            height: 17px;
            border: 1px solid #ccc;
            border-radius: 4px;">
              <span id="terms-checkbox-check" style="-webkit-tap-highlight-color: transparent;
              line-height: 1.42857;
              color: #333333;
              font-size: 13px;
              font-weight: 400;
              cursor: pointer;
              box-sizing: border-box;
              position: absolute;
              left: 2px;
              top: 3px;
              width: 12px;
              height: 7px;
              border-bottom: 3px solid #4dbecf;
              border-left: 3px solid #4dbecf;
              transform: rotate(-45deg);
              display: block;
              display: none;">
              </span>
            </span>
          </span>
          <span style="-webkit-tap-highlight-color: transparent;
          line-height: 1.42857;
          color: #333333;
          font-size: 13px;
          font-weight: 400;
          cursor: pointer;
          box-sizing: border-box;">
            <a id="open_modal" href="#ex1" rel="modal:open" style="-webkit-tap-highlight-color: transparent;
            line-height: 1.42857;
            font-size: 13px;
            font-weight: 400;
            box-sizing: border-box;
            background-color: transparent;
            outline: 0;
            text-decoration: underline;">${displayText}</a>
          </span>
        </label>
      </div>
    `;
  }

  // For jQuery version 3.3.1
  var target = [3, 3, 1];

  var current =
    typeof jQuery === "undefined"
      ? [0, 0, 0]
      : $.fn.jquery.split(".").map(function(item) {
          return parseInt(item);
        });

  if (
    current[0] < target[0] ||
    (current[0] == target[0] && current[1] < target[1])
  ) {
    loadScript(
      "//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js",
      function() {
        var jQuery1101 = jQuery.noConflict(true);
        myAppJavaScript(jQuery1101);
      }
    );
  } else {
    myAppJavaScript(jQuery);
  }
})();
