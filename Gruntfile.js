module.exports = function ( grunt ) {

	// Project configuration.
	grunt.initConfig( {
		pkg: grunt.file.readJSON( 'package.json' ),
		clean: {
			build: {
				src: [ "build/*" ]
			}
		},
		connect: {
	      options: {
	        protocol: 'http',
        	port: 8443,
	        hostname: 'localhost',
	        livereload: 35728,
	        base: 'build'
	      },
	      livereload: {
	        options: {
	          open: true
	        }
	      }
	    },
		copy: {
		  main: {
		    expand: true,
		    cwd: 'src/HTML',
		    src: '**',
		    dest: 'build/',
		  },
		},
		uglify: {
			all_src: {
				options: {
					sourceMap: false
				},
				src: 'src/JS/*.js',
				dest: 'build/<%= pkg.name %>.min.js'
			}
		},
		concat_css: {
			all: {
				src: [ "src/CSS/*.css" ],
				dest: "build/<%= pkg.name %>.css"
			},
		},
		cssmin: {
			options: {
				shorthandCompacting: false,
				roundingPrecision: -1
			},
			target: {
				files: {
					"build/<%= pkg.name %>.min.css": [ "build/<%= pkg.name %>.css" ]
				}
			}
		},
		autoprefixer: {
			options: {
				browsers: [ 'last 3 versions' ]
			},
			dist: {
				files: {
					"build/<%= pkg.name %>.css": "build/<%= pkg.name %>.css"
				}
			}

		},
		cssbeautifier: {
			files: [ "src/CSS/*.css" ]
		},
		watch: {
			javascripts: {
				files: [ 'src/**/*.js' ],
				tasks: [ 'uglify' ],
				options: {
					spawn: false,
				},
			},
			cssConcat: {
				files: [ 'src/**/*.css' ],
				tasks: [ 'cssbeautifier', 'concat_css', 'autoprefixer' ],
				options: {
					spawn: false,
				},
			},
			cssMinify: {
				files: [ 'build/<%= pkg.name %>.css' ],
				tasks: [ 'cssmin' ],
				options: {
					spawn: false,
				},
			}
		}
	} );

	// Load the plugin that provides the "uglify" task.
	grunt.loadNpmTasks( 'grunt-contrib-uglify-es' );
	grunt.loadNpmTasks( 'grunt-concat-css' );
	grunt.loadNpmTasks( 'grunt-contrib-watch' );
	grunt.loadNpmTasks( 'grunt-contrib-cssmin' );
	grunt.loadNpmTasks( 'grunt-autoprefixer' );
	grunt.loadNpmTasks( 'grunt-cssbeautifier' );
	grunt.loadNpmTasks( 'grunt-notify' );
	grunt.loadNpmTasks( 'grunt-contrib-clean' );
	grunt.loadNpmTasks( 'grunt-contrib-connect' );
	grunt.loadNpmTasks( 'grunt-contrib-copy' );

	// Default task(s).
	grunt.registerTask('serve', ['connect:livereload', 'watch']);
	grunt.registerTask( 'default', ['clean', 'copy', 'uglify', 'cssbeautifier', 'concat_css', 'autoprefixer', 'cssmin' ] );

};
